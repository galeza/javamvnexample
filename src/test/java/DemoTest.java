
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class DemoTest {

    WebDriver driver;

    @BeforeEach
    public void startBrowser() {
        String operSys = System.getProperty("os.name").toLowerCase();
        if (operSys.contains("win")) {
            System.setProperty("webdriver.chrome.driver", "libs/chromedriver.exe");
        }
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--screen-size=1200x800");
        driver = new ChromeDriver(chromeOptions);
    }
    @Test
    public void subscribersEmailValidation() {
        driver.get("https://autodemo.testoneo.com/en/");
        driver.findElement(By.cssSelector("input.btn:nth-child(1)")).click();
        String error = driver.findElement(By.cssSelector(".alert")).getText();
        assertEquals(error, "Invalid email address.");
    }
    @AfterEach
    public void tearDown() {
        driver.quit();
    }
}
